# Smart Jomok

Web-service to diagnose psychiatric trauma of a child through tales and storytelling.  We believe through telling a story
to kid, we'll be able to know kid's psychological stability and take care of it.

### Prerequisites

In order to run project in your machine you need to have several things.

- Python
- Python virtual environmment

If do not have none of those, link at the end for more information about installing Python language and virtual environment.

As an example the name of the virtualenv is venv. Install virtual environment (you can name your virtual environment)
after successfully went steps above.

```shell script
python3 -m virtualenv -p python3 venv
```
<h6>Note: This command might be different from yours. It's up to virtual environment configuration.</h6>

Activate your virtual environment

```shell script
. venv/bin/activate
```
or similar command
```shell script
source venv/bin/activate
```

### Installing

Clone the project from the repo which you have access.

<h6>Note: You need to activate your virtual environment in order to achieve all those steps below.</h6>

after cloning the project, install project dependencies from the `requirements.txt` file

```shell script
pip install -r requirements.txt
```

Create database

```shell script
python3 manage.py migrate
```

Finally, run the project on port 8000 by default

```shell script
python3 manage.py runserver
```

Check the port from your favourite browser <a href="http://localhost:8000">http://localhost:8000</a>

## Acknowledgments

* [Python](https://www.python.org/) - Python programming language
* [Python virtual environment](https://realpython.com/python-virtual-environments-a-primer/) - Python virtual environment
* [Django](https://docs.djangoproject.com/en/3.1/) - Django Framework
