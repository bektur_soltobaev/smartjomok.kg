$(document).ready(function () {

    $(".slider-1").each(function (index, elem) {
        $(this).slick({
            centerMode: true,
            slidesToShow: 3,
            asNavFor: $(this).id,
            prevArrow: "<button class=\"prev-1\"><i class=\"fas fa-chevron-left\"></i></button>",
            nextArrow: " <button class=\"next-1\"><i class=\"fas fa-chevron-right\"></i></button>",
        });
    });

    // Dropdown menu

    $('.nav__list .dropdown').hover(
        function() {
            let isHovered = $(this).is(":hover");
            if(isHovered) {
                $(this).children('ul').stop().slideDown(500);
            } else {
                $(this).children('ul').stop().slideUp(300);
            }
        })

    // Cards animation

    $('.wrapper-cards .card').hover(
        function () {
            $(this).addClass('active');
        },

        function () {
            $(this).removeClass('active');
        });

    // Search function

    $('.show-form').click(function () {
        $('.blur').show('slow');
        $(".search__popup").animate({
            display: 'block',
            width: 445,
            opacity: 1,
            top: -18,
        }, 500, "linear");
        $(".search__popup").css('display', 'block')
        $(this).css('display', 'none');
    });
    $('.search__form, .search__btn').submit(function (e) {
        $(".search__popup").animate({
            display: 'none',
            width: 0,
            top: 'auto',
            opacity: 0,
        }, 300, "linear");
        $('.blur').hide('slow');

        $('.show-form').css('display', 'block');
        $('.search__field').val('');
    });

    // Set active link

    let loc = window.location.pathname;
    $('#sitemaps').find('a').each(function () {
        $(this).toggleClass('active', $(this).attr('href') == loc);
    });

    // Insert switch language button to mobile menu
    let blockLang = `<div class="m-lang">
                        <a href="{% url 'webapp:lang' %}?lang=ky&next={{ request.build_absolute_uri }}" class="m-lang__btn" id="kyr">Кыр</a>
                        <a href="{% url 'webapp:lang' %}?lang=ru&next={{ request.build_absolute_uri }}" class="m-lang__btn" id="ru">Рус</a>
                    </div>`;


    let langBlock = $(".m-lang");

    if ($(window).width() < 860) {

        $('.dropdown').click(function(e) {
            e.preventDefault();
            $("ul .dropdown-menu").slideUp();
            $(this).find('ul').stop().slideToggle(500);
            });
    }
    // console.log($('.lang-container').has('.header__lang').length);
    console.log($('.header__lang').parent().is('.nav__list'));
    console.log($('.header__lang').parent().is('.lang-container'));

    // if ($('.lang-container').has('.header__lang')) {
    //     console.log('yes')
    // }

    $(window).resize(function () {
        if ($(window).width() < 860) {
            $('.link--submenu').attr('href', "#");

        } else {
            $(".header .nav__list").removeClass('nav-active');
            $('.link--submenu').attr('href', "{% url 'webapp:labyrinth' %}");
            $('ul .header__lang').removeClass('set-lang');

            if ($('.header__lang').parent().is('.nav__list') && $('.nav__list').has('.header__lang').length !== 0) {
                $('.nav__list .header__lang').detach();
            }
        }
    });

    $('.nav__menu-toggle').click(function() {
        if ($('.nav__list .header__lang').length === 0) {
            $('.header__lang').clone().prependTo($(".nav__list"));
        }
        $('ul .header__lang').css('display', 'flex');
        $(".header .nav__list").toggleClass('nav-active');
        $(".header .dropdown-menu").css("display", "none");
        $('#mobile-menu').toggleClass('toggle');
    })
});

