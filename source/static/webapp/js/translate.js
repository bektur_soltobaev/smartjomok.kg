let arrLang = {
    ru: {
        "maze": "Лабиринт",
        "teaching": "Обучающие",
        "parents": "Родителям",

        "good": "Доброте",
        "trying": "Старанию",
        "friendship": "Дружбе",

        "greet": "Добро пожаловать в ",
        "info": "Это сервис для детей и взрослых, который предоставляет возможность узнать и понять лучше ребёнка через сказкотерапию.",
        "btn-tail": "Перейти к сказкам",

        "ask": "Какую сказку найти?",

        "text-m": "С помощью сказок лабиринтов можно, узнать как мыслит ваш ребенок, для более легкого прохождения имеются варианты.",

        "text-t": "В этой категории собраны сказки по категориям, которые могут обучить Вашего ребенка:",
        "property": "Доброте Старанию Дружбе",

        "text-p": "Здесь собраны сказки, которые вы можете обсуждать вместе с вашим ребенком." +
            " Они побуждают к общению. Вы, как никто другой, можете помочь своему ребенку.",

        "catalog": "Каталог сказак",
        "search-result": "Результат поиска по запросу",
        "search-notfound": "Не найдено",
        "search-fail": "Нет результатов по запросу:",
    },

    kyr: {
        "maze": "Лабиринттер",
        "teaching": "Окутуучу",
        "parents": "Ата-энелерге",

        "good": "Боорукердике",
        "trying": "Аракетчилдикке",
        "friendship": "Достука",

        "greet": "Кош келиниздер",
        "info": "Бул сүйлөшүүгө аркылуу жакшыраак баланы билүү жана түшүнүүгө мүмкүндүк берет балдар жана улуулар үчүн кызмат болуп саналат.",
        "tale_btn": "Жомокторго отуу",

        "ask": "Кандай жомокту табуу?",
        "catalog": "Жомоктор каталогу",

        "text-m": "Жомоктору лабиринт жардамы менен, сиз баланын ойчулдар кантип үйрөнө алабыз, дагы жеңил өтөө үчүн жолдор бар.",
        "text-t": "Бул категорияда бала тарбиялоого мүмкүн категориялары боюнча жомокторду камтыйт:",
        "text-p": "Бул жерде сиздин бала менен бирге талкууласа болот жомоктору алынат. Алар баарлашууга үндөгөн. Сиз башка эч сыяктуу эле, бала жардам берет.",
        "search-result": "Суроо-талабы боюнча издөө жыйынтыгы",
        "search-notfound": "Табылган жок",
        "search-fail": "Талабы боюнча эч кандай жыйынтык жок:",

    }
}

let lang = "ru";

if ('localStorage' in window) {
    let usrLang = localStorage.getItem('uiLang');
    if (usrLang) {
        lang = usrLang
    }
}

$(document).ready(function () {
    $('.lang').each(function () {
        $(this).text(arrLang[lang][$(this).attr('key')]);
    });
    if (lang === 'ru') {
        $('#ru').addClass('active-lang');
    } else {
        $('#kyr').addClass('active-lang');
    }
});


$('.lang__item').click(function(e) {
    e.preventDefault();
    let lang = $(this).attr('id');

    if ('localStorage' in window) {
        localStorage.setItem('uiLang', lang);
    }

    $('.lang').each(function (i, el) {
        $(this).text(arrLang[lang][$(this).attr('key')]);
    });

    if(lang === 'ru') {
        $('#kyr').removeClass('active-lang');
        $('#ru').addClass('active-lang');
    } else {
        $('#ru').removeClass('active-lang');
        $('#kyr').addClass('active-lang');
    }
});

