from django.contrib import admin
from mptt.admin import DraggableMPTTAdmin

from webapp.models import Author, Labyrinth, TeachTale, Category, ParentsAdvice, AnswerToShow, Advice


class AuthorModelAdmin(admin.ModelAdmin):
    list_filter = ['name', 'last_name']
    search_fields = ['name', 'last_name']


class TeachTaleModelAdmin(admin.ModelAdmin):
    list_filter = ['title', 'text']
    search_fields = ['title', 'text']


class CategoryModelAdmin(admin.ModelAdmin):
    list_filter = ['title']
    search_fields = ['title']


class AdviceModelAdmin(admin.TabularInline):
    model = Advice
    fields = ['text']
    search_fields = ['text']


class ParentsAdviceModelAdmin(admin.ModelAdmin):
    list_filter = ['title', 'text']
    search_fields = ['title', 'text']
    inlines = (AdviceModelAdmin,)


admin.site.register(Author, AuthorModelAdmin)
admin.site.register(Labyrinth,DraggableMPTTAdmin,
                    list_display=( 'tree_actions', 'indented_title'),)
admin.site.register(TeachTale, TeachTaleModelAdmin)
admin.site.register(Category, CategoryModelAdmin)
admin.site.register(ParentsAdvice, ParentsAdviceModelAdmin)
admin.site.register(AnswerToShow)
admin.site.register(Advice)
