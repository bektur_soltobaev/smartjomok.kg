from webapp.models import Category


def categories(request):
    get_categories = Category.objects.all()
    return {
        'categories': get_categories
    }
