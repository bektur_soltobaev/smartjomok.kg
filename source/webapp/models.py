from django.db import models
from django.urls import reverse
from mptt.models import MPTTModel, TreeForeignKey
from django.db.models.signals import post_save
from django.dispatch import receiver


class Author(models.Model):
    name = models.CharField(max_length=25, verbose_name='Автор')
    last_name = models.CharField(max_length=30, verbose_name='Фамилия')
    position = models.CharField(max_length=50, null=True, blank=True, verbose_name='Должность')

    def __str__(self):
        return "{} {}".format(self.name, self.last_name)

    class Meta:
        verbose_name_plural = 'Авторы'


class Labyrinth(MPTTModel):
    author = models.ForeignKey('webapp.Author', on_delete=models.CASCADE, null=True, blank=True,
                               related_name='labyrinth_author', verbose_name='Автор сказки')
    title = models.CharField(max_length=255, verbose_name='Заглавие')
    text = models.TextField(max_length=7000, verbose_name='Текст')
    images = models.ImageField(upload_to='uploads', verbose_name='Картинка/Иллюстрация')
    second_text = models.TextField(max_length=7000, null=True,  verbose_name='Текст')
    parent = TreeForeignKey('self', null=True, blank=True, related_name='children',
                            on_delete=models.CASCADE)
    created_at = models.DateTimeField(auto_now_add=True)
    key_to_next_step = models.CharField(max_length=255, blank=True, null=True, 
                                        verbose_name='Ключ к этому шагу лабиринта')

    def get_absolute_url(self):
        return reverse('webapp:labyrinth_detail', kwargs={'id': self.id})

    def __str__(self):
        return self.title

    class MPTTMeta:
        order_insertion_by = ['title']

    class Meta:
        verbose_name_plural = 'Лабиринты'


class AnswerToShow(models.Model):
    tale = models.ForeignKey('webapp.Labyrinth', on_delete=models.CASCADE, related_name='answer_variants')
    text = models.TextField(max_length=2000, verbose_name='Текст')
    go_to = models.OneToOneField('webapp.Labyrinth', on_delete=models.CASCADE, related_name='answer_from')

    def __str__(self):
        return self.text

    class Meta:
        verbose_name_plural = 'Ответы к сказкам'


class TeachTale(models.Model):
    author = models.ForeignKey('webapp.Author', on_delete=models.CASCADE, null=True, blank=True,
                               related_name='teach_tale_author', verbose_name='Автор сказки')
    title = models.CharField(max_length=255, verbose_name='Заглавие')
    text = models.TextField(max_length=7000, verbose_name='Текст')
    second_text = models.TextField(max_length=7000, null=True, blank=True, verbose_name='Текст')
    images = models.ImageField(upload_to='uploads', verbose_name='Картинка/Иллюстрация')
    category = models.ForeignKey('webapp.Category', related_name='teach_tale', blank=True,
                                 null=True, on_delete=models.SET_NULL, verbose_name='Категория')
    created_at = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('webapp:teach_tale_detail', kwargs={'pk': self.pk})

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Обучающие сказки'


class Category(models.Model):
    title = models.CharField(max_length=255, verbose_name='Заглавие на русском языке')
    title_kg = models.CharField(max_length=255, null=True, verbose_name='Заглавие на кыргызском языке')

    def __str__(self):
        return self.title

    class Meta:
        verbose_name_plural = 'Категория'


class ParentsAdvice(models.Model):
    author = models.ForeignKey('webapp.Author', on_delete=models.CASCADE, null=True, blank=True,
                               related_name='parent_author', verbose_name='Автор сказки')
    title = models.CharField(max_length=255, verbose_name='Заглавие')
    text = models.TextField(max_length=7000, verbose_name='Текст')
    images = models.ImageField(upload_to='uploads', verbose_name='Картинка/Иллюстрация')
    second_text = models.TextField(max_length=7000, verbose_name='Текст')
    created_at = models.DateTimeField(auto_now_add=True)

    def get_absolute_url(self):
        return reverse('webapp:parent_detail', kwargs={'pk': self.id})

    def __str__(self):
        return self.title

    class Meta:
        ordering = ('-id',)
        verbose_name_plural = 'Советы родителям'


class Advice(models.Model):
    parent = models.ForeignKey('webapp.ParentsAdvice', null=True, on_delete=models.SET_NULL,
                               related_name='parent_advice', verbose_name='Советы')
    text = models.CharField(max_length=7000, verbose_name='Текст')

    def __str__(self):
        return self.text

    class Meta:
        verbose_name_plural = 'Советы'


@receiver(post_save, sender=Labyrinth)
def create_answer_for_labyrynth_instance(sender, instance, created, **kwargs):

    """ 
    Сигнальная функция для создания ответа к этому шагу лабиринта
    """

    if created:
        if not hasattr(instance, 'parrent'):
            return

        if instance.key_to_next_step:
            answer = AnswerToShow.objects.create(tale=instance.parent, 
                                                 text=instance.key_to_next_step,
                                                 go_to=instance)
        pass
    pass
