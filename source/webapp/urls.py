from django.urls import path

from webapp.views import (welcome_page, LanguageSwitchView, HomePage, LabyrinthListView,
                          LabyrinthDetailView, CategoryListView, TeachTaleDetailView, ParentListView,
                          ParentDetailView, CatalogListView, SearchResult
                          )

urlpatterns = [
    path('', welcome_page, name='welcome'),
    path('lang/', LanguageSwitchView.as_view(), name='lang'),
    path('home/', HomePage.as_view(), name='home'),
    path('labyrinth/', LabyrinthListView.as_view(), name='labyrinth'),
    path('labyrinth/detail/<int:id>/', LabyrinthDetailView.as_view(), name='labyrinth_detail'),
    path('teach_tale/categories/', CategoryListView.as_view(), name='teach_tale_categories'),
    path('teachtale/detail/<int:pk>/', TeachTaleDetailView.as_view(), name='teach_tale_detail'),
    path('parent/', ParentListView.as_view(), name='parent'),
    path('parent/detail/<int:pk>/', ParentDetailView.as_view(), name='parent_detail'),
    path('catalog/', CatalogListView.as_view(), name='catalog'),
    path('search/results/', SearchResult.as_view(), name='search'),
]

app_name = 'webapp'
