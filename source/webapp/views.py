from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.db.models import Q
from django.shortcuts import render, get_object_or_404, redirect
from django.utils.translation import activate
from django.views.generic import ListView, DetailView
from django import views
from itertools import chain

from webapp.models import Category, Labyrinth, TeachTale, ParentsAdvice


class LanguageSwitchView(views.View):

    def get(self, request, *args, **kwargs):
        lan_code = self.request.GET.get('lang')
        if lan_code in ('ru', 'ky'):
            activate(lan_code)
        next = request.GET.get('next')
        if next:
            return redirect(next)
        return redirect('webapp:home')


def welcome_page(request):
    return render(request, 'stories/main.html')


class HomePage(ListView):
    model = Category
    context_object_name = 'categories'
    template_name = 'stories/index.html'


class TeachTaleDetailView(DetailView):
    model = TeachTale
    context_object_name = 'teach_tale'
    template_name = 'stories/teach_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        exclude_id = self.kwargs['pk']
        context['suggestions'] = TeachTale.objects.order_by('-created_at').exclude(id=exclude_id)[:10]
        return context


class LabyrinthListView(ListView):
    model = Labyrinth
    context_object_name = 'labyrinths'
    template_name = 'stories/maze.html'
    paginate_by = 6

    def get_queryset(self):
        return Labyrinth.objects.filter(parent__isnull=True)


class LabyrinthDetailView(views.View):

    def get(self, request, id):
        labyrinth = get_object_or_404(Labyrinth, id=id)
        answers = labyrinth.answer_variants.all()
        return render(request, 'stories/detail.html', context={'labyrinth': labyrinth, 'answers': answers})


class ParentListView(ListView):
    model = ParentsAdvice
    context_object_name = 'parents'
    template_name = 'stories/parent.html'
    paginate_by = 6


class ParentDetailView(DetailView):
    model = ParentsAdvice
    context_object_name = 'parent'
    template_name = 'stories/parent_detail.html'

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        exclude_id = self.kwargs['pk']
        context['suggestions'] = ParentsAdvice.objects.order_by('-created_at').exclude(id=exclude_id)[:10]
        return context


class CategoryListView(ListView):
    model = Category
    template_name = 'stories/teach_tale.html'
    context_object_name = 'categories'


class CatalogListView(ListView):
    model = Labyrinth
    context_object_name = 'labyrinths'
    template_name = 'stories/catalog.html'
    paginate_by = 6

    def get_queryset(self):
        return Labyrinth.objects.filter(parent__isnull=True)


class SearchResult(views.View):

    def get(self, request):
        search = self.request.GET.get('search_tale')
        modified = search.capitalize()
        if modified == "":
            return render(request, 'stories/search_fail.html')
        teach_tales = TeachTale.objects.filter(Q(title__contains=modified) | Q(author__name__contains=modified))
        parents = ParentsAdvice.objects.filter(Q(title__contains=modified) | Q(author__name__contains=modified))
        labyrinth = Labyrinth.objects.filter(Q(parent__isnull=True), Q(title__contains=modified)
                                             | Q(author__name__contains=modified))
        if teach_tales or parents or labyrinth:
            results = chain(teach_tales, parents, labyrinth)
            data = self.patination(request, val=results)
            return render(request, 'stories/search_success.html', context={'data': data})
        return render(request, 'stories/search_fail.html')

    def patination(self, request, val, *args):
        paginator = Paginator(list(val), 6)
        page = request.GET.get('page', 1)
        try:
            data = paginator.page(page)
        except PageNotAnInteger:
            data = paginator.page(1)
        except EmptyPage:
            data = paginator.page(paginator.num_pages)
        return data
